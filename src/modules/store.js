
import { compose, createStore, combineReducers, applyMiddleware } from "redux";

import invoiceData from "./invoiceData/invoice.reducer";


const rootReducer = combineReducers({ invoiceData });

const store = createStore(rootReducer, undefined, compose(
	window.devToolsExtension ? window.devToolsExtension() : (f) => f
));


export default store;
