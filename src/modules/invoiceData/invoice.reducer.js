import { createReducer } from "redux-act";
import _ from 'lodash'
import * as actions from "./invoice.actions";


const initialState = {
	invoiceList: [],
};

function removeInvoice(state, id) {
	const index = state.invoiceList.findIndex(row => {
		if(row.id === id){
			return true
		}
	})
	let newState = _.cloneDeep(state);
	newState.invoiceList.splice(index, 1);
	return newState.invoiceList;
}

const reducer = {
	[actions.addNewInvoice]: (state, invoiceList) => ({
		...state,
		invoiceList: [...state.invoiceList, invoiceList],
	}),
	[actions.deleteInvoice]: (state, index) => ({
		...state,
		invoiceList:removeInvoice(state,index),
	}),
};


export default createReducer(reducer, initialState);
