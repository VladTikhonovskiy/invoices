import { createAction } from "redux-act";

export const addNewInvoice = createAction("ADD_NEW_INVOICE");
export const deleteInvoice = createAction('DELETE_INVOICE');


