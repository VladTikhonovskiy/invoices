import React, { Component } from 'react';
import { connect } from "react-redux";
import {  withRouter } from "react-router-dom";
import { array, func } from "prop-types";
import Grid from '@material-ui/core/Grid';
import * as invoiceActions from "../../modules/invoiceData/invoice.actions";
import AddInvoices from '../../components/AddInvoices/AddInvoices';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InvoicesTable from '../../components/InvoicesTable/InvoicesTable';

import "./InvoicesPage.scss";


class InvoicesPage extends Component {
    static propTypes = {
		addNewInvoice: func,
		deleteInvoice: func,
		invoiceList: array
    }

    state = {
		sortBy: ""
	}

	getSortedValue = () => {
    	const { invoiceList } = this.props;
    	const { sortBy } =this.state;
		const sorted = [...invoiceList];
		if(sortBy !== "") {
			if(sortBy === "data" ) {
				sorted.sort(function(a, b) {
					return new Date(a.data) - new Date(b.data);
				});
			}
			if( sortBy === "amount" ) {
				sorted.sort(function(a, b) {
					return +a.amount - +b.amount;
				});
			}
		}

		return sorted;
	}

	handleChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

    render() {
    	const { addNewInvoice, invoiceList, deleteInvoice } = this.props;

    	let totalAmount = 0;

		const sorted = this.getSortedValue();

		invoiceList.map(row => {
			totalAmount += +row.amount
		})

    	return (
			<Grid container>
				<AddInvoices addNewInvoice={addNewInvoice} />
				<h1> TotalAmount = {totalAmount}</h1>
				<div className={'selector-order'}>
					<Select
						value={this.state.sortBy}
						onChange={this.handleChange}
						inputProps={{
							name: 'sortBy',
							id: 'sortBy',
						}}
					>
						<MenuItem value="">
							<em>None</em>
						</MenuItem>
						<MenuItem value={"data"}>Date</MenuItem>
						<MenuItem value={"amount"}>Amount</MenuItem>
					</Select>
				</div>
				<InvoicesTable invoiceList={sorted} deleteInvoice={deleteInvoice} />
			</Grid>
    	);
    }
}
function mapStateToProps({ invoiceData }) {
	return {
		invoiceList: invoiceData.invoiceList,
	};
}

export default withRouter(connect(mapStateToProps, { ...invoiceActions })(InvoicesPage));
