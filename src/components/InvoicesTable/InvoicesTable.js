import React, { Component } from 'react';
import { array, func } from "prop-types";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './InvoicesTable.scss'

class SimpleTable extends Component {
	static propTypes = {
		invoiceList: array,
		deleteInvoice: func
	}

	onDeleteBtnClick = (id) => () => {
		this.props.deleteInvoice(id)
	}

render() {
	const { invoiceList } = this.props;
	return (
		<div className={'invoices-table'}>
			<Paper >
				<Table>
					<TableHead>
						<TableRow>
							<TableCell>Date</TableCell>
							<TableCell >Amount</TableCell>
							<TableCell >Delete</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{invoiceList.map(row => (
							<TableRow key={row.id}>
								<TableCell >{row.data}</TableCell>
								<TableCell>{row.amount}</TableCell>
								<TableCell className={"delete-btn"} onClick={this.onDeleteBtnClick(row.id)}>delete</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</Paper>
		</div>
	);
}

}

export default SimpleTable;
