import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { func } from 'prop-types';
import _ from 'lodash';

import './AddInvoices.scss';


class AddInvoices extends Component {
	static propTypes = {
		addNewInvoice: func
	}

	state = {
		amount: '',
		data: '',
		disabled: true

	};

	handleChange = name => event => {
		this.setState({ [name]: event.target.value }, () => {
			if(this.state.amount.length &&  this.state.data.length ) {
				this.setState({
					disabled: false
				})
			}else {
				this.setState({
					disabled: true
				})
			}
		});

	};

	handleBtnClick = () => {
		const { addNewInvoice } = this.props;
		const { amount, data } = this.state;

		addNewInvoice({ id: _.uniqueId("id_"), amount, data });
	}


	render() {
		const { disabled } = this.state;

		return (
			<div className={"add-invoices"}>
				<div className={"text-wrapper"}>
					<TextField
						id="date"
						label="Date"
						type="date"
						InputLabelProps={{
							shrink: true,
						}}
						onChange={this.handleChange('data')}
						margin="normal"
					/>

					<TextField
						id="standard-number"
						label="Amount"
						value={this.state.age}
						onChange={this.handleChange('amount')}
						type="number"
						InputLabelProps={{
							shrink: true,
						}}
						margin="normal"
					/>
				</div>
				<div className={'button-wrapper'}>
					<Button onClick={this.handleBtnClick} variant="contained" disabled={disabled}>
						Add
					</Button>
				</div>
			</div>

		);
	}
}


export default AddInvoices
