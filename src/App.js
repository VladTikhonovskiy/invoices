import React, { PureComponent } from 'react';
import {  withRouter, Switch, Route } from "react-router-dom";

import InvoicesPage from './container/InvoicesPage/InvoicesPage';

import routes from "./constants/routes";


class App extends PureComponent {
  render() {
    return (
          <Switch>
            <Route
                exact
                path={routes.homePage}
                component={InvoicesPage}
            />
          </Switch>
    );
  }
}

export default withRouter(App);
